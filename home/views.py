from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, logout, login
from .models import Contact
from django.contrib import messages


# Create your views here.
def index(request):
    if request.user.is_anonymous:
        return redirect('/login')
    return render(request, 'index.html')


def loginuser(request):
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            print(f"Login Successful. Welcome {username}")
            return redirect('/')
        # A backend authenticated the credentials
        else:
            return render(request, 'login.html')
    # No backend authenticated the credentials
    return render(request, 'login.html')


def logoutuser(request):
    logout(request)
    return redirect('/login')


def about(request):
    # return HttpResponse("This is my about page")
    return render(request, 'about.html')


def services(reqeust):
    # return HttpResponse("This is my services page")
    return render(reqeust, 'services.html')


def contact(request):  # request is a dictionary here
    if request.method == "POST":
        name = request.POST["name"]
        email = request.POST["email"]
        phone = request.POST["phone"]
        desc = request.POST["desc"]
        contact = Contact(name=name, email=email, phone=phone, desc=desc)
        contact.save()
        messages.success(request, 'Your message has been sent.')
    # return HttpResponse("This is my contact page")
    return render(request, 'contact.html')
